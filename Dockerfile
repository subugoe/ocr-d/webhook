FROM python:3.10-alpine

WORKDIR /app

COPY requirements.txt /app/requirements.txt
COPY webhook.env /app/webhook.env

RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

COPY ./src /app

CMD ["uvicorn", "webhook:app", "--host", "0.0.0.0", "--port", "8084"]