from fastapi import FastAPI, Request
from model import Model
import requests

with open('webhook.env', mode='r', encoding='utf-8') as env:
    TOKEN = env.read()


app = FastAPI()


@app.get('/healthz')
def get_health():
    return {'status': 'ok'}


@app.post("/webhook/release")
def release(data: Model, request: Request):
    # when a new release is triggered, GitHub sends several Webhook events
    # (created, published, released). since we don't want each release to
    # trigger 3 runs of Quiver we limit it to one.
    sha_256 = request.headers.get('X-Hub-Signature-256').split('=')[1]
    if data.action == 'published':
        print('Release detected. Starting Quiver…')
        release_tag = data.release.tag_name
        url = f'https://gitlab.gwdg.de/api/v4/projects/31070/ref/main/trigger/pipeline?token={TOKEN}&variables[RELEASE_TAG]={release_tag}&variables[SHA_256]={sha_256}'
        res = requests.post(url, timeout=10)
        return print(res.status_code)
